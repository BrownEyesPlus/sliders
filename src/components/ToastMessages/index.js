import { useEffect, useState } from 'react';

const ToastMessages = ({ messageProp }) => {
  const [toasts, setToasts] = useState([]);
  const [lastToastId, setLastToastId] = useState();

  const timeToDisappear = 3 // seconds
  const timeMilisecondToDisappear = 1000 * timeToDisappear // miliseconds

  useEffect(() => {
    if (lastToastId) {
      removeToast(lastToastId)
    }
  }, [lastToastId])

  useEffect(() => {
    if (messageProp) {
      handleShowToast(messageProp.message)
    }
  }, [messageProp])

  const handleShowToast = (message = null) => {
    if (message) {
      // Create a message and after 3 seconds it will disappear
      const newToast = {
        id: new Date().getTime(),
        message: message + new Date().getTime(),
      };

      setToasts([newToast, ...toasts]);
      setTimeout(() => {
        setLastToastId(newToast.id);
      }, timeMilisecondToDisappear);
    }
  };

  const removeToast = (id) => {
    const updatedToasts = toasts.filter((toast) => toast.id !== id);
    setToasts(updatedToasts);
  };

  return (
    <>
      <div className="toast-container">
        <div className="toast-list">
          {toasts.map((toast) => (
            <div key={toast.id} className="toast">
              {toast.message}
            </div>
          ))}
        </div>
      </div>
      <style jsx>
        {`
          .container {
            max-width: 1280px;
            margin: auto;
          }
          .toast-messages {
            min-height: 100vh;
          }
          .center {
            display: flex;
            justify-content: center;
          }
          body{
            font-size: 16px;
            font-family: 'Helvetica', 'Arial', sans-serif;
            text-align: center;
            background-color: #f8faff;
          }
          .toast-list {
            position: fixed;
            bottom: 20px;
            right: 20px;
            color: #fff;
            z-index: 9999;
            max-height: 400px;
            height: 400px;
            overflow: hidden;
            display: flex;
            flex-direction: column-reverse;
          }
          .toast {
            padding: 12px 24px;
            width: 100%;
            min-width: 200px;
            max-width: 400px;
            background-color: #333;
            border-radius: 5px;
            opacity: 0.9;
            margin-top: 1rem;
            animation-name: fade;
            animation-duration: ${timeToDisappear + 0.5}s;
            animation-timing-function: ease-in;
          }
          @keyframes fade {
            0%   {transform: translateX(110%); opacity: 1;}
            10%  { transform: translateX(110%); opacity: 1;}
            25%  {transform: translateX(0%); opacity: 1;}
            50%  {opacity: 1; max-height: 100px;}
            75%  {opacity: 0.9;}
            100% {opacity: 0;}
          }
        `}
      </style>
    </>
  )
}

export default ToastMessages;
