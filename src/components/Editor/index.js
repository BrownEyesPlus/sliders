import React, { useEffect, useRef } from 'react';
import dynamic from 'next/dynamic';

const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });
const Quill = dynamic(() => import('react-quill-with-table'), {
  ssr: false,
});

import 'quill-better-table/dist/quill-better-table.css';
const QuillBetterTable = dynamic(() => import('quill-better-table'), { ssr: false });

// if (typeof window !== 'undefined') {
//   Quill.register('modules/better-table', QuillBetterTable);
// }

const Editor = ({ value, onChange }) => {
  const reactQuillRef = useRef(null);

      console.log(Quill, 'Quill', ReactQuill)

  useEffect(() => {
    if (typeof Quill !== 'undefined') {
      console.log('Quill', Quill)
      // Quill.register('modules/better-table', QuillBetterTable);
    }
  }, [typeof Quill !== 'undefined']);

  useEffect(() => {
    if (reactQuillRef.current) {
      const editor = reactQuillRef.current.getEditor();
      editor.enable(true);
    }
  }, []);

  const modules = {
    toolbar: [
      [{ header: [1, 2, 3, false] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      ['link', 'image'],
      [{ 'align': [] }],
      ['better-table'],
      ['clean'],
    ],
    clipboard: {
      matchVisual: false,
    },
    'better-table': {
      operationMenu: {
        items: {
          unmergeCells: {
            text: 'Another unmerge cells name'
          }
          // Add more customized items if needed
        }
      }
      // Add more configurations for the better-table module if required
    },
  };

  return (
    <div>
      <ReactQuill
        ref={reactQuillRef}
        value={value}
        onChange={onChange}
        theme="snow"
        modules={modules}
      />
    </div>
  );
};

export default Editor;
