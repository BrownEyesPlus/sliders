import { useEffect, useState } from 'react'

const InfiniteVerticalCarouselList = ({ categories = [], parentWidth, settings }) => {

  const [startY, setStartY] = useState(0)

  useEffect(() => {
    const interval = setInterval(() => handleRightClick(), 3000);
    return () => clearInterval(interval);
  })

  const handleRightClick = () => {
    if (startY >= 0 && startY < categories.length - 1) {
      setStartY(startY + 1)
    } else {
      setStartY(0)
    }
  }

  const handleLeftClick = () => {
    if (startY > 0) {
      setStartY(startY - 1)
    } else {
      setStartY(categories?.length - 1)
    }
  }

  const itemWidth = (1 / settings.itemPerView) * 100

  const rightCloneCategories = categories.slice(0, settings.itemPerView + 1)

  const stepToHide = 5

  const isShowItem = (index) => {
    // const isShow = (- stepToHide) < index - startY && index - startY < stepToHide + settings.itemPerView
    // const isShow = (- stepToHide) !== index - startY - 1 || index - startY !== stepToHide + settings.itemPerView + 1
    const thisPositionY = index - startY
    const isShow = thisPositionY < - 3 || (thisPositionY > -1 && thisPositionY < settings.itemPerView)

    return isShow
  }

  const getTranslateYPercent = (index) => {
    // 5 - 0 % 4 = 1
    // 6 - 0 % 4 = 2
    // 3 - 0 % 4 = 3
    // 4 - 0 % 4 = 0
    // 4 => -2
    // 5 => -1

    // index = 4
    // startY = 0
    // length = 6
    // - (6 - 5) = -1
    // - (6 - 4) = -2

    // startY = 1
    // length = 6
    // - (6 - 5) - startY = -2
    // - (6 - 4) - startY = -3

    const thisPositionY = index - startY
    const isSwap = thisPositionY > settings.itemPerView

    if (isSwap) {
      return (- (categories?.length - thisPositionY) - startY) * 100
    } else if (thisPositionY === -1) {
      return -100
    } else if (thisPositionY < -2) {
      return (categories.length - (Math.abs(thisPositionY % categories.length))) * 100
    } else {
      return (Math.abs(thisPositionY % categories.length)) * 100
    }
  }

  console.log(startY, 'startY')


  return (
    <>
      <div className="carousel-list">
        {Array(settings.itemPerView).fill(0).map((item, index) => (
          <div className="item-frame hidden" key={index}>
            <div className="item">
              <span>
                {categories?.length}
              </span>
            </div>
          </div>
        ))}
        <div className="item-frame" style={{ transform: `translateY(${(-1 - startY) * 100}%)` }}>
          <div className="item">
            <span>
              {categories[categories.length - 1]?.data.id}
            </span>
          </div>
        </div>
        {/* {leftCloneCategories.map((item, index) => (
          <div className="item-frame" key={index} style={{ transform: `translateX(${(startY + 1 - index) * 100}%)` }}>
            <div className="item">
              <span>
                {index - categories.length + 1}
              </span>
            </div>
          </div>
        ))} */}
        {categories.map((item, index) => (
          <div
            className="item-frame"
            key={index}
            style={{
              transform: `translateY(${getTranslateYPercent(index)}%)`,
              opacity: isShowItem(index) ? '1' : '0'
            }}
          >
            <div className="item">
              <span>
                {/* {index} */}
                {item.data.id}
              </span>
            </div>
          </div>
        ))}
        {/* {rightCloneCategories.map((item, index) => (
          <div className="item-frame" key={index} style={{ transform: `translateY(${(index + categories.length - startY) * 100}%)` }}>
            <div className="item">
              <span>
                {item.data.id}
              </span>
            </div>
          </div>
        ))} */}
        {/* <div className="item-frame" style={{ transform: `translateX(${(categories?.length - startY) * 100}%)` }}>
          <div className="item">
            <span>
              {1}
            </span>
          </div>
        </div> */}
      </div>
      <div className="button-group">
        <div
          onClick={handleLeftClick}
          className="button left"
        >
          Left
        </div>
        <div
          onClick={handleRightClick}
          className="button right"
        >
          Right
        </div>
      </div>
      <style jsx>
        {`
            .carousel-list {
              position: relative;
              width: 100%;
              overflow: hidden;
            }

            .item-frame {
              position: absolute;
              top: 0;
              width: ${itemWidth}%;
              transition: 0.3s;
              padding: 0 10px;
            }
            .item-frame.hidden {
              position: unset;
            }

            .item {
              background: red;
            }

            .hidden {
              opacity: 0;
              visibility: hidden;
            }

            .button-group {
              display: flex;
              padding: 12px;
              justify-content: space-around;
            }

            .button {
              margin-top: 450px;
              min-width: 60px;
              min-height: 30px;
              display: flex;
              justify-content: center;
              align-items: center;
              background: green;
              padding: 0 24px;
              color: white;
              border-radius: 5px;
            }

            span {
              font-size: 40px;
              font-family: system-ui;
              font-weight: 600;
              color: white;
            }
          `}
      </style>
    </>
  )
}

export default InfiniteVerticalCarouselList;
