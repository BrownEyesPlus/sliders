import { useEffect, useState } from 'react'

const InfiniteCarouselList = ({ categories = [], parentWidth, settings }) => {

  const [startX, setStartX] = useState(0)

  useEffect(() => {
    const interval = setInterval(handleRightClick, 3000);
    return () => clearInterval(interval);
  })

  const handleRightClick = () => {
    if (startX >= 0 && startX < categories.length) {
      setStartX(startX + 1)
    } else {
      setStartX(1)
    }
  }

  const handleLeftClick = () => {
    if (startX > 0) {
      setStartX(startX - 1)
    } else {
      setStartX(categories?.length - settings.itemPerView)
    }
  }

  const itemWidth = (1 / settings.itemPerView) * 100

  const rightCloneCategories = categories.slice(0, settings.itemPerView + 1)
  // const leftCloneCategories = categories.slice(categories.length - settings.itemPerView - 1, settings.length)

  const stepToHide = 5

  const isShowItem = (index) => {
    const isShow = (- stepToHide) < index - startX && index - startX < stepToHide + settings.itemPerView
    return isShow
  }

  const translateXPercent = (index) => { }

  return (
    <>
      <div className="carousel-list">
        <div className="item-frame hidden">
          <div className="item">
            <span>
              {categories?.length}
            </span>
          </div>
        </div>
        <div className="item-frame" style={{ transform: `translateX(${(-1 - startX) * 100}%)` }}>
          <div className="item">
            <span>
              {categories[categories.length - 1]?.data.id}
            </span>
          </div>
        </div>
        {/* {leftCloneCategories.map((item, index) => (
          <div className="item-frame" key={index} style={{ transform: `translateX(${(startX + 1 - index) * 100}%)` }}>
            <div className="item">
              <span>
                {index - categories.length + 1}
              </span>
            </div>
          </div>
        ))} */}
        {categories.map((item, index) => (
          <div className="item-frame" key={index} style={{ transform: `translateX(${(index - startX) % (9) * 100}%)`, opacity: isShowItem(index) ? '1' : '0' }}>
            <div className="item">
              <span>
                {/* {index} */}
                {item.data.id}
              </span>
            </div>
          </div>
        ))}
        {rightCloneCategories.map((item, index) => (
          <div className="item-frame" key={index} style={{ transform: `translateX(${(index + categories.length - startX) * 100}%)` }}>
            <div className="item">
              <span>
                {item.data.id}
              </span>
            </div>
          </div>
        ))}
        {/* <div className="item-frame" style={{ transform: `translateX(${(categories?.length - startX) * 100}%)` }}>
          <div className="item">
            <span>
              {1}
            </span>
          </div>
        </div> */}
      </div>
      <div className="button-group">
        <div
          onClick={handleLeftClick}
          className="button left"
        >
          Left
        </div>
        <div
          onClick={handleRightClick}
          className="button right"
        >
          Right
        </div>
      </div>
      <style jsx>
        {`
            .carousel-list {
              position: relative;
            }

            .item-frame {
              position: absolute;
              top: 0;
              width: ${itemWidth}%;
              transition: 0.3s;
              padding: 0 10px;
            }

            .item {
              background: red;
            }

            .hidden {
              opacity: 0;
              visibility: hidden;
            }

            .button-group {
              display: flex;
              padding: 12px;
              justify-content: space-around;
            }

            .button {
              margin-top: 450px;
              min-width: 60px;
              min-height: 30px;
              display: flex;
              justify-content: center;
              align-items: center;
              background: green;
              padding: 0 24px;
              color: white;
              border-radius: 5px;
            }

            span {
              font-size: 40px;
              font-family: system-ui;
              font-weight: 600;
              color: white;
            }
          `}
      </style>
    </>
  )
}

export default InfiniteCarouselList;
