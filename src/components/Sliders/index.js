import { useState } from 'react'

const CarouselList = ({ categories = [], parentWidth, settings }) => {

  const [startX, setStartX] = useState(0)

  const handleRightClick = () => {
    if (startX >= 0 && startX < (columnQuantity - (settings.itemPerView))) {
      setStartX(startX + 1)
    }
  }

  const handleLeftClick = () => {
    if (startX > 0) {
      setStartX(startX - 1)
    }
  }

  const columnQuantity = Math.ceil(settings.itemListQuantity / settings.itemRow)
  const listWidth = ((columnQuantity) / settings.itemPerView) * 100
  const itemWidth = (1 / columnQuantity) * 100
  const marginX = startX * itemWidth

  return (
    <>
      <div className="carousel-list">
        {categories.map((item, index) => (
          <div className="item" key={index}>

          </div>
        ))}
      </div>
      <div className="button-group">
        <div
          onClick={handleLeftClick}
          className="button left"
        >
          Left
        </div>
        <div
          onClick={handleRightClick}
          className="button right"
        >
          Right
        </div>
      </div>
      <style jsx>
        {`
            .carousel-list {
              transform: translateX(-${marginX}%);
              position: absolute;
              width: ${listWidth}%;
              // flex-wrap: wrap;
              display: flex;
              gap: 12px;
              transition: .4s;
              gap: 10px;
            }

            .item {
              width: calc(${itemWidth}%);
              background: red;
              min-height: 200px;
              // flex: 1 1 auto;
            }

            .button-group {
              display: flex;
              padding: 12px;
              justify-content: space-around;
            }

            .button {
              margin-top: 450px;
              min-width: 60px;
              min-height: 30px;
              display: flex;
              justify-content: center;
              align-items: center;
              background: green;
              padding: 0 24px;
              color: white;
              border-radius: 5px;
            }
          `}
      </style>
    </>
  )
}

export default CarouselList;
