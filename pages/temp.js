import { useState, useRef } from 'react';

const slides = [
  'https://via.placeholder.com/500x300?text=Slide%201',
  'https://via.placeholder.com/500x300?text=Slide%202',
  'https://via.placeholder.com/500x300?text=Slide%203',
];

const Slider = () => {
  const [startX, setStartX] = useState(null);
  const [isDragging, setIsDragging] = useState(false);
  const [currentSlide, setCurrentSlide] = useState(0);
  const sliderRef = useRef(null);

  const handleTouchStart = (e) => {
    setStartX(e.touches[0].clientX);
    setIsDragging(true);
  };

  const handleTouchMove = (e) => {
    if (!isDragging) return;
    const x = e.touches[0].clientX;
    const distance = x - startX;
    sliderRef.current.style.transform = `translateX(${currentSlide * 100 + distance }%)`;
  };

  const handleTouchEnd = () => {
    setIsDragging(false);
    const moveThreshold = 50;
    const distance = startX - startX;

    if (distance > moveThreshold && currentSlide < slides.length - 1) {
      setCurrentSlide(currentSlide + 1);
    } else if (distance < -moveThreshold && currentSlide > 0) {
      setCurrentSlide(currentSlide - 1);
    }

    sliderRef.current.style.transform = `translateX(-${currentSlide * 100}%)`;
  };

  return (
    <div>
      <h1>Slider Example</h1>
      <div className="slider">
        <div
          ref={sliderRef}
          className={"slideContainer"}
          onTouchStart={handleTouchStart}
          onTouchMove={handleTouchMove}
          onTouchEnd={handleTouchEnd}
        >
          {slides.map((slide, index) => (
            <div
              key={index}
              className={`slide ${index === currentSlide ? 'active' : ''
                }`}
            >
              <img src={slide} alt={`Slide ${index}`} />
            </div>
          ))}
        </div>
      </div>

      <style jsx>
        {`
          .slider {
            width: 80%;
            margin: auto;
            overflow: hidden;
            position: relative;
          }

          .slideContainer {
            display: flex;
            transition: transform 0.3s ease; /* Hiệu ứng trượt */
          }

          .slide {
            width: 100%;
            flex-shrink: 0;
            touch-action: pan-y; /* Cho phép kéo ngang */
          }

          .slide img {
            width: 100%;
          }

          .active {
            opacity: 1;
          }
        `}
      </style>
    </div>
  );
};

export default Slider;
