import Editor from '@/src/components/Editor';
import React, { useState } from 'react';
// import Editor from '../components/Editor'; // Assuming the editor component is in components folder

const EditorPage = () => {
  const [editorValue, setEditorValue] = useState('');

  const handleEditorChange = (value) => {
    setEditorValue(value);
  };

  return (
    <div>
      <h1>Editor Page</h1>
      <Editor value={editorValue} onChange={handleEditorChange} />
    </div>
  );
};

export default EditorPage;
