import InfiniteCarouselList from '@/src/components/InfiniteSliders';
import CarouselList from '@/src/components/Sliders';

const list = [
  {
    data: {
      id: 0,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 1,
      slug: 'hello',
      name: ' Alaba trap asdasdasd a a da asdasdasccwq wescsa fa'
    }
  },
  {
    data: {
      id: 2,
      slug: 'hello',
      name: ' Alaba trap asdas asdasd a dasdasd'
    }
  },
  {
    data: {
      id: 3,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 4,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 5,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 6,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 7,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 8,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 9,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
]

const containerDiv = 0

const settings = {
  itemPerView: 4,
  itemListQuantity: list.length || 3,
  itemRow: 1,
}

const CarouselListApp = () => {

  return (
    <>
      <div className="slider-frame">
        <div className="slider">
          <InfiniteCarouselList categories={list} parentWidth={containerDiv.clientWidth} settings={settings} />
        </div>
      </div>

      <style jsx>
        {`
          .slider-frame {
            padding: 0 100px;
            padding-top: 24px;
            // padding-right: -20px;

            box-sizing: border-box';
            min-height: 100vh;
            // display: flex;
          }
          .slider {
            position: relative;
          }
        `}
      </style>
    </>
  )
}

export default CarouselListApp;
