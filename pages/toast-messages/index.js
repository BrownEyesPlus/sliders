import { useState } from 'react';

import BubblyButton from '@/src/components/Button/BubblyButton';
import ToastMessages from '@/src/components/ToastMessages';

const ToastMessagesPage = ({ }) => {
  const [message, setMessage] = useState({});

  return (
    <>
      <ToastMessages messageProp={message} />
      <div className="container toast-messages">
        <div className="center">
          <BubblyButton
            className="bubbly-button"
            onClick={() => setMessage({ success: false, message: 'dau xanh ra ma cai thang kia' })}
          >
            Click me!
          </BubblyButton>
        </div>
      </div>
      <style jsx>
        {`
          .container {
            max-width: 1280px;
            margin: auto;
          }
          .toast-messages {
            min-height: 100vh;
          }
          .center {
            display: flex;
            justify-content: center;
          }
          body{
            font-size: 16px;
            font-family: 'Helvetica', 'Arial', sans-serif;
            text-align: center;
            background-color: #f8faff;
          }
          .toast {
            position: fixed;
            bottom: 20px;
            left: 50%;
            transform: translateX(-50%);
            background-color: #333;
            color: #fff;
            padding: 10px 20px;
            border-radius: 5px;
            opacity: 0.9;
            z-index: 9999;
          }
        `}
      </style>
    </>
  )
}

export default ToastMessagesPage;
