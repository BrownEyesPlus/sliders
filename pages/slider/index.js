import CarouselList from '@/src/components/Sliders';

const list = [
  {
    data: {
      id: 0,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 1,
      slug: 'hello',
      name: ' Alaba trap asdasdasd a a da asdasdasccwq wescsa fa'
    }
  },
  {
    data: {
      id: 2,
      slug: 'hello',
      name: ' Alaba trap asdas asdasd a dasdasd'
    }
  },
  {
    data: {
      id: 3,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 4,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 5,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 6,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 7,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
  {
    data: {
      id: 8,
      slug: 'hello',
      name: ' Alaba trap'
    }
  },
]

const containerDiv = 0

const settings = {
  itemPerView: 2,
  itemListQuantity: list.length || 3,
  itemRow: 1,
}

const CarouselListApp = () => {

  return (
    <>
      <div className="slider">
        <CarouselList categories={list} parentWidth={containerDiv.clientWidth} settings={settings} />
      </div>
      <style jsx>
        {`
          .slider {
            margin-top: 24px;
            min-height: 100vh;
            position: relative;
          }
        `}
      </style>
    </>
  )
}

export default CarouselListApp;
