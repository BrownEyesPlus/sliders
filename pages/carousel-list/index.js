import Link from 'next/link';
import { useRouter } from 'next/router';
import { useRef, useState } from 'react';
// import Link from '../components/Link';

const CarouselList = () => {
  const router = useRouter();
  const [scrollPosition, setScrollPosition] = useState(0);
  const [isDragging, setIsDragging] = useState(false);
  const [isMoving, setIsMoving] = useState(false);
  const [startX, setStartX] = useState(0);
  const listRef = useRef(null);

  const categories = [
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
    {
      id: 1,
      slug: '',
      name: ' dau xanh day la cach test slider cua toi',
    },
  ]

  const handleMouseDown = (event) => {
    setIsDragging(true);
    setStartX(event.pageX);
    listRef.current.style.cursor = 'grabbing';
  };

  const handleMouseMove = (event) => {
    if (isDragging) {
      const deltaX = event.pageX - startX;
      listRef.current.scrollLeft = scrollPosition - deltaX;
      setIsMoving(true);
    }
  };

  const handleMouseUp = () => {
    setTimeout(() => {
      setIsMoving(false);
    }, 10);
    setIsDragging(false);
    setScrollPosition(listRef.current.scrollLeft);
    listRef.current.style.cursor = 'grab';
  };

  return (
    <>
      <div className="categories">
        <div
          className="category-list"
          ref={listRef}
          onMouseDown={handleMouseDown}
          onMouseMove={handleMouseMove}
          onMouseUp={handleMouseUp}
          onMouseLeave={handleMouseUp}
        >
          <div href={isMoving ? router.asPath : '/'}>
            <a className={`category-item ${router.asPath === '/' ? 'active' : ''}`}>Trang chủ</a>
          </div>
          {categories.map(c => (
            <div href={isMoving ? router.asPath : ('/' + c.id + '-' + c.slug)} key={c.id}>
              <a className={`category-item ${router.asPath === ('/' + c.id + '-' + c.slug) ? 'active' : ''}`} key={c.id}>{c.name}</a>
            </div>
          ))}
        </div>
      </div>
      <style jsx>
        {`
          .categories {
            width: 100%;
            background-color: #FD9426;
            height: 2.75rem;
            user-select: none;
          }
          .category-list {
            max-width: calc(1170px - 4rem);
            width: 100%;
            overflow: auto;
            white-space: nowrap;
            margin: auto;
            display: flex;
            gap: .75rem;
          }
          .category-list::-webkit-scrollbar-track {
            display: none;
          }

          .category-list::-webkit-scrollbar {
            display: none;
          }

          .category-list::-webkit-scrollbar-thumb {
            display: none;
          }
          .category-item {
            position: relative;
            width: 107px;
            min-width: 107px;
            max-width: 107px;
            text-align: center;
            font-weight: 600;
            line-height: 2.75rem;
            color: #fff;
            text-decoration: none;
            -webkit-user-drag: none;
          }
          .category-item.active {
            font-weight: 800;
          }
          .category-item.active:before {
            content: "";
            height: 2px;
            width: 100%;
            background: #fff;
            position: absolute;
            bottom: 0;
            left: 0;
          }
        `}
      </style>
    </>
  )
}

export default CarouselList;
